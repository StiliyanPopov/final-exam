package Negative;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class NewAccountRegistration {

    public static WebDriver driver;

    @BeforeClass
    public void setUp() {

        System.setProperty("webdriver.edge.driver", "E:\\WebDrivers\\MicrosoftWebDriver.exe");
        driver = new EdgeDriver();


        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void newAccountRegistrationNegative() {

        driver.findElement(By.xpath("//li[2]//a//span[1]")).click();

        WebElement register = driver.findElement(By.xpath("//div[@id='top-links']//ul//li[2]//ul//li[1]//a"));
        register.click();
        String actualTitle = driver.getTitle();

        assertEquals(actualTitle, "Pragmatic Test Store", "Actual and Expected result discrepancy");

        WebElement firstName = driver.findElement(By.id("input-firstname"));
        firstName.sendKeys("");

        WebElement lastName = driver.findElement(By.id("input-lastname"));
        lastName.sendKeys("");

        WebElement email = driver.findElement(By.id("input-email"));
        email.sendKeys("");

        WebElement telephone = driver.findElement(By.id("input-telephone"));
        telephone.sendKeys("");

        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("");

        WebElement confirm = driver.findElement(By.id("input-confirm"));
        confirm.sendKeys("");

        WebElement radioButtonYes = driver.findElement(By.name("newsletter"));

        if (radioButtonYes.isSelected()) {
            radioButtonYes.click();
        }

        assertFalse(radioButtonYes.isSelected());

        WebElement agreeCheckbox = driver.findElement(By.name("agree"));
        if (agreeCheckbox.isSelected()) {
            agreeCheckbox.click();
        }

        assertFalse(agreeCheckbox.isSelected());


        driver.findElement(By.xpath("//div[@id='content']//form//div//div//input[2]")).click();



        WebElement verify = driver.findElement(By.xpath("//div[@class='alert alert-danger alert-dismissible']"));
        String actualRegistrationTitle = verify.getText();
        assertEquals(actualRegistrationTitle, " Warning: You must agree to the Privacy Policy!");




        WebElement firstNameError = driver.findElement(By.xpath("//fieldset[@id='account']//div[2]//div//div"));
        String firstNameText = firstNameError.getText();

        assertEquals(firstNameText, "First Name must be between 1 and 32 characters!", "Actual and Expected result in First Name is different");

        WebElement lastNameError = driver.findElement(By.xpath("//fieldset[@id='account']//div[3]//div//div"));
        String lastNameText = lastNameError.getText();

        assertEquals(lastNameText, "Last Name must be between 1 and 32 characters!", "Actual and Expected result in Last Name is different");

        WebElement emailError = driver.findElement(By.xpath("//fieldset[@id='account']//div[4]//div//div"));
        String emailText = emailError.getText();
        assertEquals(emailText, "E-Mail Address does not appear to be valid!", "Actual and Expected result in email is different");

        WebElement telephoneError = driver.findElement(By.xpath("//fieldset[@id='account']//div[5]//div//div"));
        String telephoneText = telephoneError.getText();
        assertEquals(telephoneText, "Telephone must be between 3 and 32 characters!", "Actual and Expected result in telephone is different");


        WebElement passwordError = driver.findElement(By.xpath("//fieldset[2]//div[1]//div//div"));
        String passwordText = passwordError.getText();
        assertEquals(passwordText, "Password must be between 4 and 20 characters!", "Actual and Expected result in password is different");


    }
}
