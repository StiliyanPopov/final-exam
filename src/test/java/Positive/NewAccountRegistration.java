package Positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * This class create new account registration
 */

public class NewAccountRegistration {

    public static WebDriver driver;

    /**
     * This method open the browser and going to the page for testing
     */

    @BeforeClass
    public void setUp() {

        System.setProperty("webdriver.edge.driver", "E:\\WebDrivers\\MicrosoftWebDriver.exe");
        driver = new EdgeDriver();


        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg");

    }

    /**
     * This method close the browser after test
     */

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    /**
     * This method register new account and type the needed values for all fields
     */

    @Test
    public void newAccountRegistrationPositive() {

        driver.findElement(By.xpath("//li[2]//a//span[1]")).click();

        WebElement register = driver.findElement(By.xpath("//div[@id='top-links']//ul//li[2]//ul//li[1]//a"));
        register.click();
        String actualTitle = driver.getTitle();

        assertEquals(actualTitle, "Pragmatic Test Store", "Actual and Expected result discrepancy");

        driver.findElement(By.id("input-firstname")).sendKeys("Onche");

        driver.findElement(By.id("input-lastname")).sendKeys("Bonche");

        driver.findElement(By.id("input-email")).sendKeys("georgi_iii@abv.bg");

        driver.findElement(By.id("input-telephone")).sendKeys("0888888888");

        driver.findElement(By.id("input-password")).sendKeys("1234567890");

        driver.findElement(By.id("input-confirm")).sendKeys("1234567890");

        WebElement radioButtonYes = driver.findElement(By.name("newsletter"));

        if (!radioButtonYes.isSelected()) {
            radioButtonYes.click();
        }

        assertTrue(radioButtonYes.isSelected());

        WebElement agreeCheckbox = driver.findElement(By.name("agree"));
        if (!agreeCheckbox.isSelected()) {
            agreeCheckbox.click();
        }

        assertTrue(agreeCheckbox.isSelected());


        WebElement verify = driver.findElement(By.xpath("//div[@id='content']//form//div//div//input[2]"));
        verify.click();
        String actualRegistrationTitle = driver.getTitle();

        assertEquals(actualRegistrationTitle, "Register Account", "Actual and Expected result discrepancy");

    }
}
